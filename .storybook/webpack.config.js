const _ = require("lodash");
const projectConfig = require("../webpack.config");

module.exports = (storybookBaseConfig) => {

    let result = Object.assign(storybookBaseConfig, {});
    result.module.rules = storybookBaseConfig.module.rules.concat(projectConfig.module.loaders);
    result.resolve.extensions = _.union(storybookBaseConfig.resolve.extensions, projectConfig.resolve.extensions);
    result.resolve.modules = storybookBaseConfig.resolve.modules.concat(projectConfig.resolve.modules);
    // resolve : {
    //     extensions : _.union(storybookBaseConfig.resolve.extensions, projectConfig.resolve.extensions),
    //     modules : storybookBaseConfig.resolve.modules //.resolve.modules.concat(projectConfig.resolve.modules)
    // }
    return result;

};