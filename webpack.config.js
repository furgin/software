let webpack = require('webpack');
let path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: {
        app: './src/App'
    },
    output: {
        path: __dirname + "/generated",
        filename: '[name].js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.DedupePlugin(), //dedupe similar code
        new webpack.optimize.UglifyJsPlugin(), //minify everything
        new webpack.optimize.AggressiveMergingPlugin()//Merge chunks
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    babelrc: false,
                    presets: [ ["env", {modules: false}], "react", "stage-2" ]
                },
                exclude: [
                    path.resolve(__dirname, 'node_modules/')
                ]
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader",
                exclude: path.resolve(__dirname, 'node_modules/')
            }
        ]
    },
    resolve: {
        extensions: [".js", ".less"],
        modules: [
            path.resolve("./src/client"),
            path.resolve("./node_modules")
        ]
    }
};
