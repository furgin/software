import React from "react";

class Field extends React.Component {

    render() {
        return (
            <div className="field common">
                <div className="details">
                    <div className="heading">{this.props.heading}:</div>
                    <div className="value">{this.props.value}</div>
                    <div className="needs">{this.props.needs && "Needs " + this.props.needs}</div>
                </div>
            </div>
        );
    }

}
export default Field
