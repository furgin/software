import React from "react";

class Field extends React.Component {

    render() {
        return (
            <div className="field common">
                <div className={"action "+this.props.state}
                     onClick={() => {
                         if (this.props.state === 'enabled')
                             this.props.actionClick()
                     }}>{this.props.action}</div>
            </div>
        );
    }

}
export default Field
