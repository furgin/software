import React from "react";

class Value extends React.Component {

    render() {
        return (
            <div className="value common">
                <div className="details">
                    <div className="label">{this.props.label}:</div>
                    <div className="value">{this.props.value}</div>
                </div>
            </div>
        );
    }

}
export default Value