import React from "react";

class Value extends React.Component {

    render() {
        let width = this.props.value + "%";
        return (
            <div className="value progress common">
                <div className="details">
                    <div className="label">{this.props.label}:</div>
                    <div className="percentage">
                        <div className="bar" style={{width: width}}/>
                    </div>
                </div>
            </div>
        );
    }

}
export default Value