import React from "react";
import AppContainer from "./AppContainer";
import store from "./Store";
import {Provider} from "react-redux";
import {storiesOf} from "@storybook/react";
import Data from './Data'

class AppContainerLoader extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log(JSON.stringify(this.props._data));
        store.dispatch({'type': 'TEST_SETUP', payload: this.props._data});
    }

    render() {
        return (
            <Provider store={store}>
                <AppContainer/>
            </Provider>
        );
    }
}

const defaultState = Data.initialState;

storiesOf('App', module)
    .add('start',
        () => <AppContainerLoader
            _data={defaultState}/>)
    .add('9 features',
        () => <AppContainerLoader
            _data={{...defaultState, features: 9}}/>)
    .add('buy team',
        () => <AppContainerLoader
            _data={{...defaultState, features: 11, releases: 1, users: 1, money: 10}}/>)
    .add('buy lots of teams',
        () => <AppContainerLoader
            _data={{...defaultState, features: 11, releases: 1, users: 1, money: 500}}/>)
    .add('cooking with gas',
        () => <AppContainerLoader
            _data={{
                ...defaultState,
                features: 5000,
                releases: 5,
                users: 5000,
                teams: 100,
                money: 2000000,
                servers: { build:100 }
            }}/>)
;
