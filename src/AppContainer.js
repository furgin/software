import React from "react";
import Field from "./components/Field";
import Action from "./components/Action";
import Progress from "./components/Progress";
import Value from "./components/Value";
import Flags from "./Flags";
import {connect} from "react-redux"

import Layout from './layout/Layout'
import Row from './layout/Row'
import Column from './layout/Column'
import StandardRow from './StandardRow';

require('./AppContainer.less');

class AppContainer extends React.Component {

    componentDidMount() {
        this.refreshIntervalId = setInterval(() => {
            // software teams auto write software
            let autoFeatures = this.props.game.teams * 0.1; // TODO team effectiveness
            if (autoFeatures > 0) {
                this.props.writeFeature(autoFeatures);
            }
            // build servers auto release software
            let releaseCost = Flags.releases.next(this.props.game);
            let autoReleases = (this.props.game.servers.build / releaseCost) * 10; // TODO server effectiveness
            if (autoReleases > 0 && Flags.releases.status(this.props.game) === 'enabled') {
                this.props.doBuilding(autoReleases);
            }
            // complete builds create releases
            if (this.props.game.building >= 100) {
                this.props.doRelease(1);
            }
            // attract new users
            let newUsers = this.props.game.features * 0.0001; // TODO marketing effectiveness
            if (newUsers > 0) {
                this.props.addUsers(newUsers);
            }
            // profit
            let newMoney = Math.floor(this.props.game.users) * 0.05; // TODO price per user
            if (newMoney > 0) {
                this.props.addMoney(newMoney);
            }

        }, 10);
    }

    componentWillUnmount() {
        clearInterval(this.refreshIntervalId);
    }

    render() {
        let releasesStatus = Flags.releases.status(this.props.game);
        let teamsStatus = Flags.teams.status(this.props.game);
        let teamCost = Flags.teams.next(this.props.game);
        let buildServerStatus = Flags.servers.build.status(this.props.game);
        let buildServerCost = Flags.servers.build.next(this.props.game);

        return (
            <Layout>
                <Row>
                    <Column>
                        <h1>Software</h1>
                    </Column>
                </Row>

                <StandardRow
                    action={<Action state={releasesStatus}
                                    actionClick={() => {
                                        this.props.doBuilding(25)
                                    } }
                                    action="Build"/>}
                    col1={<Field heading="Releases"
                                 value={this.props.game.releases.toFixed(0)}
                                 needs={Flags.releases.next(this.props.game).toFixed(0) + " features"}/>}
                    col2={<Value label="Money" value={"$" + this.props.game.money.toFixed(2)}/>}
                />

                <StandardRow
                    col1={this.props.game.building > 0 && (
                        <Progress label="Building" value={this.props.game.building}/>)}
                    col2={<Value label="Users" value={Math.floor(this.props.game.users)}/>}
                />

                <StandardRow
                    action={<Action state="enabled"
                                    actionClick={() => {
                                        this.props.writeFeature(1)
                                    }}
                                    action="Write"/>}
                    col1={<Field heading="New Features"
                                 value={this.props.game.features.toFixed(0)}/>}
                />

                {teamsStatus != 'hidden' && (
                    <StandardRow
                        action={<Action state={teamsStatus}
                                        actionClick={() => {
                                            this.props.buyTeam(teamCost)
                                        }}
                                        action="Buy"/>}
                        col1={<Field heading="Teams"
                                     value={this.props.game.teams}

                                     needs={"$" + teamCost.toFixed(2)}/>}
                    />
                )}
                {buildServerStatus != 'hidden' && (
                    <StandardRow
                        action={<Action state={buildServerStatus}
                                        actionClick={() => {
                                            this.props.buyBuildServer(buildServerCost)
                                        }}
                                        action="Buy"/>}
                        col1={<Field heading="Build Server"
                                     value={this.props.game.servers.build.toFixed(0)}
                                     needs={"$" + buildServerCost.toFixed(2)}/>}
                    />
                )}
            </Layout>
        );
    }

}

const mapStateToProps = (state, ownProps) => ({
    game: state,
});

const mapDispatchToProps = (dispatch) => ({
    writeFeature: (amount) => {
        dispatch({type: 'ADD_FEATURE', payload: {amount: amount}})
    },
    buyTeam: (cost) => {
        dispatch({type: 'BUY_TEAM', payload: {cost: cost, amount: 1}});
    },
    buyBuildServer: (cost) => {
        dispatch({type: 'BUY_BUILD_SERVER', payload: {cost: cost, amount: 1}});
    },
    doRelease: (amount) => {
        dispatch({type: 'ADD_RELEASE', payload: {amount: amount}});
    },
    doBuilding: (amount) => {
        dispatch({type: 'ADD_BUILDING', payload: {amount: amount}});
    },
    addUsers: (amount) => {
        dispatch({type: 'ADD_USER', payload: {amount: amount}})
    },
    addMoney: (amount) => {
        dispatch({type: 'ADD_MONEY', payload: {amount: amount}})
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)
