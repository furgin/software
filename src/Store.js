import {createStore, applyMiddleware, combineReducers} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import reducer from './Reducer';
import {createLogger} from 'redux-logger'
import Data from './Data'

const composeEnhancers = composeWithDevTools({});

const logger = createLogger({
    predicate: (getState, action) => action.type !== "TICK"
});

const store = createStore(
    reducer,
    Data.initialState,
    composeEnhancers(
        applyMiddleware(
            thunkMiddleware,
            promiseMiddleware(),
            // logger
        )));

export default store;