
const Data = {
    initialState: {
        time: 0,
        features: 0.0,
        releases: 0.0,
        teams: 0.0,
        users: 0.0,
        servers: {
            build: 0.0
        },
        building: 0,
        money: 0
    }
};

export default Data;