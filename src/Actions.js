const Actions = {
    tick: (state) => {
        return function (dispatch) {
            dispatch({type: 'TICK'});
            let autoFeatures = state.teams;
            if (autoFeatures > 0) {
                dispatch({type: 'ADD_FEATURE', payload: {amount: autoFeatures}})
            }
        }
    },
};

export default Actions;