import React from "react";
import PropTypes from 'prop-types';

require('./Layout.less');

class Row extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let totalWidth = this.context.width;
        console.log(totalWidth);
        let fillCount = 0;

        React.Children.forEach(this.props.children, (child)=>{
            if(!child.props.width) {
                fillCount = fillCount+1;
            } else {
                totalWidth = totalWidth-child.props.width;
            }
        });

        const childrenWithExtraProp = React.Children.map(this.props.children,
            child => {
                // let childWidth = (100.0 / React.Children.count(this.props.children))+"%";
                let childWidth = (totalWidth/fillCount)+"px";
                if(child.props.width) {
                    childWidth = child.props.width+"px";
                }
                return React.cloneElement(child, {
                    _width: childWidth
                });
            });

        return (
            <div className="row">
                {childrenWithExtraProp}
            </div>
        );
    }

}

Row.contextTypes = {
    width: PropTypes.number,
    height: PropTypes.number
};

export default Row;
