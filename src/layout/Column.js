import React from "react";
import PropTypes from 'prop-types';

require('./Layout.less');

class Column extends React.Component {

    render() {
        return (
            <div className={"column " + this.props.className}
                 style={{width:this.props._width}}>
                {this.props.children}
            </div>
        );
    }

}

Column.contextTypes = {
    color: PropTypes.string
};

export default Column;
