import React from "react";
import {storiesOf} from "@storybook/react";
import Layout from './Layout'
import Row from './Row'
import Column from './Column'

require("./Layout.stories.less");

storiesOf('Layout', module)
    .add('single cell',
        () => <Layout><Row><Column className="red"><h1>Single Cell</h1></Column></Row></Layout>)
    .add('two rows',
        () => <Layout>
            <Row><Column className="red"><h1>Row 1</h1></Column></Row>
            <Row><Column className="green"><h1>Row 2</h1></Column></Row>
        </Layout>)
    .add('two colums',
        () => <Layout>
            <Row>
                <Column className="red"><h1>Column 1</h1></Column>
                <Column className="green"><h1>Column 2</h1></Column>
            </Row>
        </Layout>)
    .add('three colums',
        () => <Layout>
            <Row>
                <Column className="red"><h1>Column 1</h1></Column>
                <Column className="green"><h1>Column 2</h1></Column>
                <Column className="blue"><h1>Column 3</h1></Column>
            </Row>
        </Layout>)
    .add('1 fixed column',
        () => <Layout>
            <Row>
                <Column width="200" className="red"><h1>Fixed</h1></Column>
                <Column className="green"><h1>Fill</h1></Column>
                <Column className="blue"><h1>Fill</h1></Column>
            </Row>
        </Layout>)
    .add('2 fixed columns',
        () => <Layout>
            <Row>
                <Column width="200" className="red"><h1>Fixed</h1></Column>
                <Column className="green"><h1>Fill</h1></Column>
                <Column width="200" className="red"><h1>Fixed</h1></Column>
                <Column className="blue"><h1>Fill</h1></Column>
            </Row>
        </Layout>)
    .add('complex layout',
        () => <Layout>
            <Row><Column className="red"><h1>Row 1</h1></Column></Row>
            <Row>
                <Column className="red"><h1>Column 1</h1></Column>
                <Column className="green"><h1>Column 2</h1></Column>
                <Column className="blue"><h1>Column 3</h1></Column>
            </Row>
            <Row>
                <Column className="green"><h1>Column 1</h1></Column>
                <Column className="red"><h1>Column 2</h1></Column>
            </Row>
            <Row>
                <Column className="red"><h1>Column 1</h1></Column>
                <Column className="green"><h1>Column 2</h1></Column>
                <Column className="blue"><h1>Column 3</h1></Column>
            </Row>
        </Layout>)
;
