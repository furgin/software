import React from "react";
import PropTypes from 'prop-types';

require('./Layout.less');

class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    updateDimensions() {
        if (this.div) {
            this.width = this.div.offsetWidth;
            this.height = this.div.offsetHeight;
            // this.forceUpdate();
        }
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions)
    }

    getChildContext() {
        console.log("getChildContext: ",this.width, this.height);
        return {width: this.width, height: this.height};
    }

    render() {
        return (
            <div ref={(div) => { this.div = div; }}
                 className="layout">{this.props.children}</div>
        );
    }
}

Layout.childContextTypes = {
    width: PropTypes.number,
    height: PropTypes.number
};

export default Layout
