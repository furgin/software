import Flags from "./Flags";

const reducer = (state = {}, action) => {
    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'TEST_SETUP':
            newState = JSON.parse(JSON.stringify(action.payload));
            return newState;
        case 'TICK':
            newState.time = newState.time + 1;
            return newState;
        case 'ADD_FEATURE':
            newState.features = newState.features + action.payload.amount;
            return newState;
        case 'ADD_RELEASE':
            newState.features = newState.features - Flags.releases.next(newState);
            newState.releases = newState.releases + action.payload.amount;
            newState.building = 0;
            return newState;
        case 'ADD_USER':
            newState.users = newState.users + action.payload.amount;
            return newState;
        case 'ADD_BUILDING':
            newState.building = newState.building + action.payload.amount;
            return newState;
        case 'ADD_MONEY':
            newState.money = newState.money + action.payload.amount;
            return newState;
        case 'BUY_TEAM':
            newState.money = newState.money - action.payload.cost;
            newState.teams = newState.teams + action.payload.amount;
            return newState;
        case 'BUY_BUILD_SERVER':
            newState.money = newState.money - action.payload.cost;
            newState.servers.build = newState.servers.build + action.payload.amount;
            return newState;
        default:
            return newState;
    }
};

export default reducer;