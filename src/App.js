
import React from 'react';
import ReactDOM from 'react-dom';
import AppContainer from './AppContainer';
import store from './Store';
import Actions from './Actions';
import {Provider} from "react-redux"

ReactDOM.render((
    <Provider store={store}>
        <AppContainer/>
    </Provider>
), document.getElementById("app"));

