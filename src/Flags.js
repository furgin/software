// const scale = (base, val, z) => (base + ((val * (val ^ z))));
const scale = (base, val, z) => (base + Math.pow(val, z));

const Flags = {
    releases: {
        next: (state) => {
            return scale(1, Math.floor(state.releases), 3)
        },
        status: (state) => {
            if (state.features < Flags.releases.next(state)) {
                return "disabled";
            } else {
                return "enabled";
            }
        }
    },
    teams: {
        next: (state) => {
            return scale(1, state.teams, 4)
        },
        status: (state) => {
            if (state.releases < 1) {
                return "hidden";
            } else if (state.money < Flags.teams.next(state)) {
                return "disabled";
            } else {
                return "enabled";
            }
        }
    },
    servers: {
        build: {
            next: (state) => {
                return scale(1, state.servers.build, 4.5)
            },
            status: (state) => {
                if (state.teams < 1) {
                    return "hidden";
                } else if (state.money < Flags.servers.build.next(state)) {
                    return "disabled";
                } else {
                    return "enabled";
                }
            }
        }
    }
};

export default Flags;