import React from "react";
import Row from "./layout/Row";
import Column from "./layout/Column";

class StandardRow extends React.Component {
    render() {
        return (
            <Row>
                <Column width="130">{this.props.action}</Column>
                <Column>{this.props.col1}</Column>
                <Column>{this.props.col2}</Column>
            </Row>
        )
    }
}

export default StandardRow;